import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Task 1
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Please enter an integer: ");
            try {
                int input = scanner.nextInt();
                System.out.println("The square of " + input + ": " + (input * input));
                isValid = true;
            } catch (InputMismatchException e) {
                System.out.println("That is not an integer.");
                scanner.nextLine();
            }
        }

        // Task 2
        System.out.print("What is current temperature? ");
        int temp = scanner.nextInt();

        try {
            testInput(temp);
        } catch (InvalidInputException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void testInput(int input) throws InvalidInputException {
        if (!(input >= 20 && input <= 30)) {
            throw new InvalidInputException("The temperature is not normal.");
        } else {
            System.out.println("The temperature is fine." +
                    "No need to water plants.");
        }
    }
}

class InvalidInputException extends Exception {
    public InvalidInputException(String errorMessage) {
        super(errorMessage);
    }
}